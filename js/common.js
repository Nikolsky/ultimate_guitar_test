jQuery(function($) {
    function setHeight() {
        $('.content').css({
            'min-height': $(window).height() + 'px'
        });
    };
    setHeight();
    $(window).resize(setHeight);

    $('#form .signin_note a').click(function () {
        $('#form .signin_note,#form .sign_in').fadeOut(400);
        $('#form form button').html('Sign up');
    });


    /*input invalid*/
    $(document).bind('change', function(e){
        if( $(e.target).is(':invalid') && $(e.target).val()!="" ){
            $(e.target).parent().addClass('invalid');
        } else {
            $(e.target).parent().removeClass('invalid');
        }
    });
});

/*Submit*/
function success() {
    var data   = $('form').serialize();
    $.ajax({
        type: 'POST',
        url: 'authorization.php',
        data: data,
        success: function(data) {
            $('.success').fadeIn(400, function () {
                $('#form .success .icon').addClass('animate');
                setTimeout(function () {
                    $('.success h2').fadeIn(400);
                    setTimeout(function () {
                        $('#form .signin_note,#form .sign_in').fadeIn(0);
                        $('#form form button').html('Sign in');
                        $('input').val('');
                        $('.success, .success h2').fadeOut(600);
                    }, 5000)
                }, 600);
            });
        },
        error:  function(xhr, str){
            alert('Возникла ошибка: ' + xhr.responseCode);
        }
    });
}